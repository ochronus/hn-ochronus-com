package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	netlify "github.com/netlify/open-api/go/porcelain"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	var options = netlify.DeployOptions{
		SiteID:  "siteid",
		Dir:     "yolo",
		IsDraft: false,
		Title:   "new deploy",
	}
	fmt.Println(options)
	fmt.Println(os.Getenv("NETLIFY_API_KEY"))
}
